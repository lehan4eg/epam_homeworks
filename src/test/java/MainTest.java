import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class MainTest {
    private ByteArrayOutputStream output = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(output));
    }

    @Test
    public void firstQuarter() {
    String expect = "I";
    String result = Main.getQuarter(1,1);
        Assert.assertEquals(expect,result);
    }

    @Test
    public void secondQuarter() {
        String expect = "II";
        String result = Main.getQuarter(-1,1);
        Assert.assertEquals(expect,result);
    }

    @Test
    public void thirdQuarter() {
        String expect = "III";
        String result = Main.getQuarter(-1,-1);
        Assert.assertEquals(expect,result);
    }

    @Test
    public void fourthQuarter() {
        String expect = "IV";
        String result = Main.getQuarter(1,-1);
        Assert.assertEquals(expect,result);
    }

    @Test
    public void xAxis() {
        String expect = "ось x\r\n";
        Main.itsOnAxis(0,1);
        String result = output.toString();
        Assert.assertEquals(expect,result);
    }

    @Test
    public void yAxis() {
        String expect = "ось y\r\n";
        Main.itsOnAxis(1,0);
        String result = output.toString();
        Assert.assertEquals(expect,result);
    }

    @Test
    public void xYAxis() {
        String expect = "центр оси координат\r\n";
        Main.itsOnAxis(0,0);
        String result = output.toString();
        Assert.assertEquals(expect,result);
    }

}
