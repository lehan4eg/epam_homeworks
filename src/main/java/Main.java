import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите X");
        double x = scanner.nextDouble();
        System.out.println("Введите Y");
        double y = scanner.nextDouble();
        if (!itsOnAxis(x, y)) {
            System.out.println(getQuarter(x, y));
        }
    }

    static boolean itsOnAxis(double x, double y) {
        if (x == 0) {
            if (y == 0) {
                System.out.println("центр оси координат");
                return true;
            }
            System.out.println("ось x");
            return true;
        } else if (y == 0) {
            System.out.println("ось y");
            return true;
        } else {
            return false;
        }
    }

    static String getQuarter(double x, double y) {
        if (x > 0) {
            return y > 0 ? "I" : "IV";
        } else {
            return y > 0 ? "II" : "III";
        }
    }
}
